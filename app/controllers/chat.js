module.exports.startChat = function(application, req, res) {

    const dataForm = req.body
    req.assert('name', 'Name is required').notEmpty()
    req.assert('name', 'Name field must contain 3 letters').len(3, 15)

    const errors = req.validationErrors()

    if(errors) {
        res.render('index', {validation: errors})
        return
    }

    console.log(dataForm);
    res.render('chat')
}