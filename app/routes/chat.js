module.exports = function(application) {
    application.post('/chat', (req, res) => {
        application.app.controllers.chat.startChat(application, req, res);
    })

    application.get('/chat', (req, res) => {
        application.app.controllers.chat.startChat(application, req, res);
    })
}